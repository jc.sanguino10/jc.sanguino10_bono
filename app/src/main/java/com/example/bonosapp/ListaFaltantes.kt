package com.example.bonosapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_lista_faltantes.*
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.time.LocalDateTime
import java.time.Month



class ListaFaltantes : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_faltantes)
        val text_view = TextView(this)
        text_view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f)
        childrenScrollView.addView(text_view)
        text_view.text= "Cargando los estudiantes que no vinieron a clase..."
        var textList = ""
        var url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=120"
        val queue = Volley.newRequestQueue(this)
        val stringRequest = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                val jsOb = JSONObject(response)
                val array = jsOb.optJSONArray("documents")
                val path = getExternalFilesDir(null)
                val fichero = File(path, "listaEstudiantes")
                fichero.mkdir()
                for (i in 0..(array.length()-1)) {
                    val name = array.getJSONObject(i).getJSONObject("fields").getString("name")
                    val codigo = array.getJSONObject(i).getJSONObject("fields").getString("code")
                    textList += name + "-" + codigo
                    if (i != array.length() - 1) {
                        textList += ";"
                    }
                }
                text_view.text = textList.replace(";", "\n")
            },
            Response.ErrorListener { text_view.text = "La base de datos no responde"})
        queue.add(stringRequest)
        val today = LocalDateTime.now()
        var mes = Month.AUGUST
        var date = ""
        var i = 1
        var temp = true
        while(i <= mes.maxLength() && temp )
        {
            var day: LocalDateTime = LocalDateTime.of(2019, mes ,i,0,0,0)
            if(day.dayOfYear > today.dayOfYear)
            {
                temp = false
            }
            if(day.dayOfWeek.value == 1 || day.dayOfWeek.value == 2 || day.dayOfWeek.value == 5)
            {
                date += i.toString() + "/" + mes.value + "/2019" + ";"
                Log.println(Log.INFO,"dia", i.toString())
            }
            if(i == mes.maxLength())
            {
                mes = Month.of(mes.value+1)
                i = 1
            }
            i++
        }


        val dates: Array<String> = date.split(";").toTypedArray()
        spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, dates)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                println("No hay nada seleccionado")
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

            }
        }

        btnCsv.setOnClickListener {
            try{
                val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "archivoPrueba.txt")
                FileOutputStream(file).write(textList.toByteArray())
                Toast.makeText(this,"Se creo el archivo en la carpeta de descarga",Toast.LENGTH_SHORT).show()
            }
            catch(e : Exception)
            {
                Toast.makeText(this,e.message ,Toast.LENGTH_SHORT).show()
            }

        }
    }

    fun stringJson (str:String): String {
        val cad = str.split(":")[1]
        return cad.substring(1,cad.length-2)
    }
}